# jvm

#### 介绍
Java虚拟机相关

#### 类的加载过程
_加载(Loading)->链接(Linking)->初始化(Initialization)_
- 加载：
  - 通过一个类的全限定名获取定义此类的二进制字节流
  - 将这个字节流所代表的静态存储结构转化为方法区的运行时数据结构
  - **在内存中生成一个代表这个类的java.lang.Class对象**，作为方法区这个类的各种数据的访问入口
- 链接：
  - 验证
    - 目的在于确保Class文件的字节流中包含信息符合当前虚拟机要求，保证被加载类的正确性，不会危害虚拟机自身安全
    - 主要包括四种验证，文件格式验证、元数据验证、字节码验证、符号引用验证
  - 准备
    - 为类变量分配内存并设置该类变量的默认初始值，即零值
    - **这里不包含用final修饰的static，因为final在编译的时候就会分配了，准备阶段会显示初始化**
    - **这里不会为实例变量分配初始化**，类变量会分配在方法区中，而实例变量是会随着对象一起分配到Java堆中
  - 解析
    - 将常量池内的符号引用转换为直接引用的过程
    - 事实上，解析操作往往会伴随着jvm在执行完初始化之后再执行
    - 符号引用就是一组符号来描述所引用的目标，符号引用的字面量形式明确定义在《Java虚拟机规范》的Class文件格式中，直接引用就是直接指向目标的指针、相对偏移量或一个简介定位到目标的句柄
    - 解析动作主要针对类或接口、字段、类方法、接口方法、方法类型等。对应常量池中CONSTANT_Class_info、CONSTANT_Fieldref_info、CONSTANT_Methodref_info等
- 初始化：
  - **初始化阶段就是执行类构造器方法\<clinit>()的过程**
  - 此方法不需要定义，是javac编译器自动收集类中的所有类变量的赋值动作和静态代码块中的语句合并而来
  - 构造器方法中指令按语句在源文件中出现的顺序执行
  - **\<clinit>()不同于类的构造器**。（关联：构造器是虚拟机视角下的\<init>()）
  - 若该类具有父类，JVM会保证子类的\<clinit>()执行前，父类的\<clinit>()已执行完毕
  - 虚拟机必须保证一个类的\<clinit>()方法在多线程下被同步加锁

#### 类加载器的分类

- JVM支持两种类型的类加载器，分别是引导类加载器(Bootstrap ClassLoader)和自定义类加载器(User-Defind ClassLoader)
- 从概念上来讲，自定义类加载器一般是指程序中由开发人员自定义的一类类加载器，但是Java虚拟机规范没有那么定义，而是将所有派生于抽象类ClassLoader的类加载器都划分为自定义类加载器
  ![类加载器分类](https://img-blog.csdnimg.cn/802a058e84d64e3882e3bdfbcfef9c9a.png#pic_center)
  **这里的四者之间的关系是包含关系，不是上层下层，也不是父子类的继承关系**
##### 虚拟机自带的加载器
- 启动类加载器（引导类加载器，Bootstrap ClassLoader）
  - 这个类加载器使用C/C++语言实现，嵌套在JVM内部
  - 它用来加载Java的核心库(JAVA_HOME/jre/lib/rt.jar、resourses.jar或sun.boot.class.path路径下的内容)，用于提供JVM自身需要的类
  - 并不继承自java.lang.ClassLoader，没有父加载器
  - 加载扩展类和应用程序类加载器，并指定为他们的父类加载器
  - 处于安全考虑，Bootstrap启动类加载器只加载包名为java、javax、sun等开头的类
- 扩展类加载器
  - Java语言编写，由sun.misc.Launcher$ExtClassLoader实现
  - 派生于ClassLoader类
  - 父类加载器为启动类加载器
  - 从java.ext.dirs系统属性所指定的目录中加载类库，或从JDK的安装目录的jre/lib/ext子目录（扩展目录）下加载类库。如果用户创建的JAR放在此目录下，也会自动由扩展类加载器加载
    -应用程序类加载器（系统类加载器，AppClassLoader）
  - Java语言编写，由sun.misc.Launcher$AppClassLoader实现
  - 派生于ClassLoader类
  - 父类加载器为扩展类加载器
  - 他负责加载环境变量classpath或系统属性，java.class.path指定路径下的类库
  - 该类加载是程序中默认的类加载器，一般来说，Java应用的类都是由它来完成加载的
  - 通过ClassLoader#getSystemClassLoader()方法可以获得该类加载器
##### 自定义类加载器
- 为什么要自定义类的加载器
  - 隔离加载类
  - 修改类加载的方式
  - 扩展加载源
  - 防止源码泄露
- 用户自定义加载器实现步骤：
  - 开发人员可以通过继承抽象类java.lang.ClassLoader类的方式，实现自己的类加载器，以满足一些特殊的需求
  - 在JDK1.2之前，在自定义类加载器是，总会去集成ClassLoader类并重写loadClass()方法，从而实现自定义的类加载类，但是在JDK1.2之后已不再建议用户去覆盖loadClass()方法，而是建议把自定义的类加逻辑写在findClass()中
  - 在编写自定义类加载器时，如果没有太过于复杂的需求，可以直接继承URLClassLoader类，这样就可以避免自己去编写findClass()方法即获取字节码流的方式，使自定义类加载器编写更加简洁
#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
