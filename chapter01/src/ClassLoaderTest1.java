import sun.misc.Launcher;
import sun.security.ec.point.AffinePoint;
import sun.security.util.CurveDB;

import java.net.URL;
import java.security.Provider;
import java.util.Properties;

public class ClassLoaderTest1 {

    public static void main(String[] args) {

        System.out.println("************启动类加载器************");
        //获取BootstrapClassLoader能够加载的api的路径
        URL[] urLs = Launcher.getBootstrapClassPath().getURLs();
        for (URL urL : urLs) {
            System.out.println(urL.toExternalForm());
        }
        //从上面的路径中随意选择一个类，来看看它的类加载器是什么
        ClassLoader classLoader = Provider.class.getClassLoader();//null
        System.out.println(classLoader);

        System.out.println("************扩展类加载器************");
        String properties = System.getProperty("java.ext.dirs");
        for (String s : properties.split(";")) {
            System.out.println(s);
        }

        //从上面的路径中随意选择一个类，来看看它的类加载器是什么
//        ClassLoader classLoader1 = CurveDB.class.getClassLoader();
        ClassLoader classLoader1 = AffinePoint.class.getClassLoader();//sun.misc.Launcher$ExtClassLoader@677327b6
        System.out.println(classLoader1);

    }

}
